# zulip

A CLI zulip client

## Installation

[Download the statically linked
version](https://0xacab.org/sutty/zulip-client/-/raw/antifascista/zulip-static?inline=false)
and copy it anywhere in your PATH, or compile and install.

## Compile

Install Crystal >= 1.6.2 and run:

```bash
make zulip-static # For the run-anywhere version
make zulip # For the local-only version
make install # Compile and install, requires sudo
```

Docker is needed to compile the static version.

## Usage

Create a Generic bot on Zulip and copy their e-mail address and API key.

```bash
ZULIP_KEY="the API key" \
zulip -u https://sutty.zulipchat.com \
      -b "name-bot@sutty.zulipchat.com" \
      -s "Stream name" \
      -t "Topic" \
      "message"
```

The message can be read from standard input.

The Zulip server can be the "sutty" part if it's hosted at https://zulipchat.com

Other environment variables:

```bash
ZULIP_URL="https://sutty.zulipchat.com"
ZULIP_BOT="name-bot@sutty.zulipchat.com"
ZULIP_STREAM="Stream name"
ZULIP_TOPIC="Topic"
```

## Contributing

1. Open an issue at <https://0xacab.org/sutty/zulip-client/-/issues>
2. Create an issue-X branch where X is the issue number.
3. Create a merge request

## Contributors

- f - creator and maintainer
