require "option_parser"
require "http/client"
require "base64"
require "json"

zulip_url = ENV.fetch("ZULIP_URL", "")
username = ENV.fetch("ZULIP_BOT", "")
password = ENV.fetch("ZULIP_KEY", "")
code = false

params = URI::Params.new
params["type"] = "stream"
params["to"] = ENV.fetch("ZULIP_STREAM", "")
params["topic"] = ENV.fetch("ZULIP_TOPIC", "")

OptionParser.parse do |p|
  p.banner = "Usage: zulip -u https://sutty.zulipchat.com -s stream -t topic \"message\""

  p.on("-h", "--help", "Help") do
    puts p
    exit
  end

  p.on("-u URL", "--url=URL", "Zulip URL") do |url|
    zulip_url = url
  end

  p.on("-s NAME", "--stream=NAME", "Stream") do |name|
    params["to"] = name
  end

  p.on("-t NAME", "--topic=NAME", "Topic") do |name|
    params["topic"] = name
  end

  p.on("-b BOT", "--bot=BOT", "Bot e-mail") do |bot|
    username = bot
  end

  p.on("-c", "--code", "Add Markdown backticks to input") do
    code = true
  end
end

params["content"] = String.build do |io|
  io << "```\n" if code

  if ARGV.empty?
    io << STDIN.gets_to_end if ARGV.empty?
  else
    io << ARGV.join(" ")
  end

  io << "\n```\n" if code
end

zulip_url = "https://#{zulip_url}.zulipchat.com" unless zulip_url.starts_with? "https://"

url = URI.parse(zulip_url)
url.path = "/api/v1/messages"
url.query_params = params
url.user = username
url.password = password

response = HTTP::Client.post(url)

unless response.success?
  if response.body.starts_with? "{"
    msg = JSON.parse(response.body)["msg"]
  else
    msg = response.body
  end

  STDERR.puts msg
  exit 1
end
