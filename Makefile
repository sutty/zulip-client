zulip-static: src/zulip-static.cr src/zulip.cr
	docker run --rm -it -v $(PWD):/workspace -w /workspace crystallang/crystal:latest-alpine \
    crystal build $< --static --release
	strip --strip-all $@

zulip: src/zulip.cr
	crystal build --release $<
	strip --strip-all $@

install: zulip
	install -o root -g root -m 755 -d $< $(PREFIX)/usr/bin/zulip
